const tinhTrungBinh = function () {
  const soThuc1 = document.getElementById(`sothuc1`).value * 1;
  const soThuc2 = document.getElementById(`sothuc2`).value * 1;
  const soThuc3 = document.getElementById(`sothuc3`).value * 1;
  const soThuc4 = document.getElementById(`sothuc4`).value * 1;
  const soThuc5 = document.getElementById(`sothuc5`).value * 1;

  const phepTinh = (soThuc1 + soThuc2 + soThuc3 + soThuc4 + soThuc5) / 5;

  if (
    soThuc1 != "" &&
    soThuc2 != "" &&
    soThuc3 != "" &&
    soThuc4 != "" &&
    soThuc5 != ""
  ) {
    document.getElementById(
      `result-ex2`
    ).innerHTML = `Trung bình 5 số thực: ${phepTinh}`;
  } else {
    return (document.getElementById(
      `result-ex2`
    ).innerHTML = `Vui lòng kiểm tra lại thông tin!`);
  }

  $("#form-2").submit(function (e) {
    e.preventDefault(); // <==stop page refresh==>
  });
};
