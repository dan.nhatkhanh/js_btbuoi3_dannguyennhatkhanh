const tinhChuVi = function () {
  const chieuDai = document.getElementById(`chieudai`).value;
  const chieuRong = document.getElementById(`chieurong`).value;
  const chuVi = (Number(chieuDai) + Number(chieuRong)) * 2;

  if (chieuDai != "" && chieuRong != "") {
    document.getElementById(
      `result-ex4`
    ).innerHTML = `Chu vi hình chữ nhật: ${chuVi}`;
  } else {
    return (document.getElementById(
      `result-ex4`
    ).innerHTML = `Vui lòng kiểm tra lại thông tin!`);
  }

  $("#form-4").submit(function (e) {
    e.preventDefault(); // <==stop page refresh==>
  });
};

const tinhDienTich = function () {
  const chieuDai = document.getElementById(`chieudai`).value * 1;
  const chieuRong = document.getElementById(`chieurong`).value * 1;
  const dienTich = chieuDai * chieuRong;

  if (chieuDai != "" && chieuRong != "") {
    document.getElementById(
      `result-ex4`
    ).innerHTML = `Diện tích hình chữ nhật: ${dienTich}`;
  } else {
    return (document.getElementById(
      `result-ex4`
    ).innerHTML = `Vui lòng kiểm tra lại thông tin!`);
  }

  $("#form-4").submit(function (e) {
    e.preventDefault(); // <==stop page refresh==>
  });
};
